# Testowanie funkcji bitucketa

## Code review i pull-requesty

Celem niniejszego projektu jest przetestowanie funkcjonalnosci
pull-requestow w bitbuckecie, w celu wprowadzenia praktyki
recenzowania kodu zrodlowego we wszystkich projektach.

## Struktura repozytorium:
* projekt powinien posiadac nastepujace galezie:
  * master (development), do ktorego merge'owane sa wszystkie nowe branche
  * release, do ktorego merge'owany jest branch master w momencie,
  gdy program zostaje wydany do klienta (cf. git workflow)
  * feature-branche i bug-fixy. Nie nalezy nigdy commitowac
  bezposrednio do galezi master -- wszystkie commity powinny
  byc scalane wylacznie za posrednictwem pull-requestow
  
## Dyscyplina prac nad projektem

Glowne zalozenie jest takie, ze zaden commit nie powinien
trafic do galezi rozwojowej, jezeli nie zostanie zweryfikowany
przez programiste, ktory nie jest autorem tego kodu.
Idealnie kazdy commit powinien miec dwoch reviewerow.
Aby wymusic tego rodzaju dyscypline, warto wprowadzic
funkcje osoby odpowiedzialnej za zmerge'owanie danego
pull-requesta. Osoba taka nie moze byc autorem zmian,
ktorych ma dotyczyc dany code-review.

Jezeli zrobiles pull-requesta i nie wiesz, kto powinien zmerge'owac
Twoje zmiany, skontaktuj sie ze swoim managerem, a juz on cos
na pewno wymysli. (W razie czego polecam Michala Janke na
code reviewera, poniewaz (a) lubi sie czepiac oraz (b)
zna sie na gicie)

## Jak przeprowadzac code-review
Podreczniki opisujace code-review zalecaja, zeby recenzowac
maksymalnie 400 linii kodu na godzine, i zeby pojedyncza
sesja code-review nie trwala dluzej niz godzine. Z tego powodu,
w celu maksymalizacji sprawnosci, warto rozbic duzy problem
na mniejsze podproblemy/podetapy, ktore moga byc przetwarzane
i recenzowane osobno. (Idealnie by bylo, zeby calkowita liczba
zmian w pojedynczym pull-requescie nie przekraczala owych
400 linii kodu)

W trakcie przeprowadzana code-review warto korzystac
z listy rzeczy do sprawdzenia. Liste te warto aktualizowac,
dopisujac do niej czesto pojawiajace sie/typowe bledy,
zas dla kazdego znalezionego defektu warto sprobowac
zaklasyfikowac go do ktoregos z podpunktow listy (zas
w razie braku odpowiedniej kategorii dopisac nowa).
Przykladowa lista moze miec nastepujaca postac:

- czy wprowadzone zmiany sa zgodne z wymaganiami?
- czy program jest napisany zgodnie ze standardami kodowania?
- czy wszystkie bledy zostaly obsluzone?
- czy funkcje sprawdzaja poprawnosc swoich argumentow?
- czy dla nowych funkcji zostaly napisane testy jednostkowe?
- czy wszystkie zaalokowane zasoby zostaja zwolnione?
- czy wszystkie znalezione defekty zostaly zaklasyfikowane
wedlug powyzszej listy?

W trakcie prac nad code-review kwestie pomniejsze mozna
pisac w komentarzach, ale dla takich mogacych wymagac
wiecej pracy warto zalozyc nowe zadania na JIRA (jako
pod-zdania od glownego zadania, ktorego dotyczy dana
galaz)

Oczywiscie, dopuszczalne (a nawet jak najbardziej wskazane)
jest konsultowanie jakosci kodu zrodlowego z kolegami
w trakcie prac nad nim, z pominieciem dodatkowych narzedzi
jak JIRA czy Bitbucket, jednak nie zmienia to faktu, ze
scalanie zrodel do repozytorium powinno obowiazkowo
odbywac sie za posrednictwem mechanizmu pull-request.
